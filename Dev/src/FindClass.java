import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FindClass {

	private Scanner scanner;
	private final List<Integer> numberList;


	public FindClass () {
		numberList = new ArrayList<>();
		scanner = new Scanner(System.in);
	}


	public void gameStart() {
		
		System.out.println("To begin the game press Y");
		String confirmationString = scanner.next();
		

		System.out.println("Enter the first number");
		int firstNumber = scanner.nextInt();
		addList(firstNumber);

		System.out.println("Enter the second number");
		int secondNumber = scanner.nextInt();
		addList(secondNumber);

		System.out.println("Enter the third number");
		int thirdNumber = scanner.nextInt();
		addList(thirdNumber);

		System.out.println("Enter the fourth number");
		int fourthNumber = scanner.nextInt();

		int resultAdd = addNumbers(firstNumber, secondNumber, thirdNumber);

		averageNumbers(resultAdd, fourthNumber);
		
		scanner.close();

	}


	public int addNumbers(int number, int number2, int number3) {
		int addition = number + number2 + number3;
		System.out.println("Here is the sum of the numbers you have chosen " + addition);
		return addition;
	}

	public double averageNumbers(int sum, int fourthNumber) {
		double average = sum / fourthNumber;
		System.out.println("Here is the result of the division "+ average);
		return  average;
	}


	public void addList(int number) {
		numberList.add(number);
	}

	public void getAllIntegers() {
		System.out.println(numberList.toString());
	}

	public void listSize() {
		System.out.println(numberList.size());
	}



}
